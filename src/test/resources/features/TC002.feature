Feature: Scenarios for petstore

@TC002 @Suite1 
 Scenario Outline: Get a pet
   Given I have a petId to get a pet with "<petId>"  
   And I use pet header
   When I create get request to get a petId
   Then I get status code 200 from db 
   Then the schema from response matches "get_pet.petId.json"

   Examples:

     | petId               |  
     | 9223372036854775807 |  