Feature: Scenarios for petstore

@TC001 @Suite1 
 Scenario Outline: Add a new pet
   Given I have data to create a pet with "<categoryName>", "<name>", "<photoUrls>", "<tagName>" and "<status>"   
   And I use pet header
   When I create post request to create a pet
   Then I get status code 200 from db
   Then the schema from response matches "post_pet.json"

   Examples:

     | categoryName | name   | photoUrls  |  tagName |  status    | 
     | stringA       | doggie | stringB     | stringC   | available  |  