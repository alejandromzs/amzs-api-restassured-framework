#A Java RestAssured framework


### Pre-requisites
```
  1)  Java 8 JDK, Maven
```
 


### Steps to run  
```
   modify the tags to runy in the file "src/test/java/runner/Runtest"
   from cmd run "mvn clean compile test"
```

### Review Results
```
  Go to folder "target"
  Open cucumber-reports.html report with chrome browser 
  Go to folder "target/timeline"
  Open index.html report with chrome browser to show time consumed	
```