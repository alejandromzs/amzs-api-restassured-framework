package petstore.dataEntities;

import org.json.JSONArray;
import org.json.JSONObject;

public class Pet {

    private String category;
    private String name;
    private String photoUrls;
    private String tags;
    private String status;
    private String petId = "0"; 
     
    public Pet() {
    }

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhotoUrls() {
		return photoUrls;
	}

	public void setPhotoUrls(String photoUrls) {
		this.photoUrls = photoUrls;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
 
    
	public String getPetPayload() {
		JSONObject pet = new JSONObject();
		pet.put("id", Long.parseLong(petId));
		
		JSONObject category = new JSONObject();
		category.put("id", 0);
		category.put("name", this.category);
		
		pet.put("category",category);
		
		pet.put("name", this.name);
		
		JSONArray photoUrls = new JSONArray();
		photoUrls.put(this.photoUrls);

		pet.put("photoUrls", photoUrls);
		
		JSONObject tagsObj = new JSONObject();
		tagsObj.put("id", 0);
		tagsObj.put("name", this.tags);
		
		JSONArray tagsArray = new JSONArray();
		tagsArray.put(tagsObj);
		
		pet.put("tags",tagsArray);
		
		pet.put("status",this.status);
		
		int spacesToIndentEachLevel = 2;
		System.out.println("pet payload"); 
		System.out.println(pet.toString(spacesToIndentEachLevel));
		return pet.toString(); 
	}

	public void setPetId(String petId) {
		this.petId = petId;
		
	}
	
	public String getPetId() {
		return petId;
	}

  
    
}
