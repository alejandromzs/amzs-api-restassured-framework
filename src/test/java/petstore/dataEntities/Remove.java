package petstore.dataEntities;

import org.json.JSONArray;
import org.json.JSONObject;

public class Remove {
public static void main(String[] args) {
  
	int spacesToIndentEachLevel = 2;
 
	JSONObject pet = new JSONObject();
	pet.put("id", 0);
	
	JSONObject category = new JSONObject();
	category.put("id", 0);
	category.put("name", "string");
	
	pet.put("category",category);
	
	pet.put("name", "doggie");
	
	JSONArray photoUrls = new JSONArray();
	photoUrls.put("string");

	pet.put("photUrls", photoUrls);
	
	JSONObject tagsObj = new JSONObject();
	tagsObj.put("id", 0);
	tagsObj.put("name", "string");
	
	JSONArray tagsArray = new JSONArray();
	tagsArray.put(tagsObj);
	
	pet.put("tags",tagsArray);
	
	pet.put("status","available");
	 
	System.out.println(pet.toString(spacesToIndentEachLevel));
}
}
