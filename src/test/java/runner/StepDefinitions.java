package runner;

import org.junit.Assert;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response; 
import io.restassured.specification.RequestSpecification;
import petstore.dataEntities.Pet;
import zippopotam.dataEntities.PostalCode;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class StepDefinitions {
	Pet pet  = new Pet();
	PostalCode postalCode = new PostalCode();
	private Response response;
	private RequestSpecification request;
	
	
	@Given("I have a country {string} and a postal code {string}")
	public void i_have_a_country_and_a_postal_code(String countryValue, String postalCodeValue) {
		postalCode.setCountry(countryValue);
		postalCode.setPostalCode(postalCodeValue); 
	}
	
	
	@Given("I use country header")
	public void i_use_country_header() {
		   request = given().header("Content-Type", "application/json");
	}
	@When("I create get request to get a postal information")
	public void i_create_get_request_to_get_a_postal_information() {
		response = request.when()
				.get("https://zippopotam.us/"+postalCode.getCountry()+"/"+postalCode.getPostalCode());
	}
	
	 
	@Then("I get status code 200 from db")
	public void i_get_status_code_from_db(){
	   Assert.assertEquals(200, response.getStatusCode());
	}
	
	@Then("the schema from response matches {string}")
	public void the_schema_from_response_matches(String jsonSchema) {
		// Retrieve the body of the Response 
		System.out.println("Response Body is: " + response.getBody().asString()); 
	 
	      response.then().assertThat()
	    .body(matchesJsonSchemaInClasspath("schemas/"+ jsonSchema));
	}
	
	
	
	
	
	
	
	
	
	
	
	@Given("I have data to create a pet with {string}, {string}, {string}, {string} and {string}")
	public void i_have_data_to_create_a_pet_with_and(String category, String name, String photoUrls, String tags, String status) {
		pet.setCategory(category);
		pet.setName(name);
		pet.setPhotoUrls(photoUrls);
		pet.setStatus(status);
		pet.setTags(tags);
	}
	
	
	@When("I create post request to create a pet")
	public void i_create_post_request_to_create_a_pet() {
		response = request.when()
				.body(pet.getPetPayload())
				.post("https://petstore.swagger.io/v2/pet");
	}
 
	 @And("I use pet header")
	   public void i_use_planet_header(){
	       request = given().header("Content-Type", "application/json");
	   }
	
 
	
	
	
	@Given("I have a petId to get a pet with {string}")
	public void i_have_a_pet_id_to_get_a_pet_with(String petId) {
	   pet.setPetId(petId);
	}
	
	@When("I create get request to get a petId")
	public void i_create_get_request_to_get_a_pet_id() {
		response = request.when()
				.get("https://petstore.swagger.io/v2/pet/"+pet.getPetId());
	}
	 
	@Given("I have data to update a pet with {string},{string}, {string}, {string}, {string} and {string}")
	public void i_have_data_to_update_a_pet_with_and(String petId, String categoryName, String name, String photoUrls, String tagName, String status) {
		pet.setPetId(petId);		
		pet.setCategory(categoryName);
		pet.setName(name);
		pet.setPhotoUrls(photoUrls);
		pet.setStatus(status);
		pet.setTags(tagName);
		
	}
	@When("I create put request to update a pet")
	public void i_create_put_request_to_update_a_pet() {
		response = request.when()
				.body(pet.getPetPayload())
				.put("https://petstore.swagger.io/v2/pet/");
	}
	
}
