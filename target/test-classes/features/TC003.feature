Feature: Scenarios for petstore

@TC003 @Suite1 
 Scenario Outline: Update a pet
   Given I have data to update a pet with "<petId>","<categoryName>", "<name>", "<photoUrls>", "<tagName>" and "<status>"   
   And I use pet header
   When I create put request to update a pet
   Then I get status code 200 from db
   Then the schema from response matches "put_pet.json"
	
   Examples:

     | petId                 |categoryName   | name     | photoUrls    |  tagName   |  status    | 
     | 9223372036854775807   |stringA        | doggie   | stringB      | stringC    | available  |  
     | 9223372036854775807   |stringA2       | doggie2  | stringB2     | stringC2   | available  |  